const express = require('express');
const app = express();
const port = 8080;

app.use(express.json());
app.use(express.urlencoded(
    extended = false
))

let planetList = [
    {
        id: 0,
        name: "Venus",
        size: 300,
        dateDiscovered: 1954
    }
];

app.get('/', (req, resp) => {
    
    resp.json(planetList);
    resp.end();
});

app.post('/add', (req, resp) => {
    let { id, name, size, dateDiscovered } = req.body;

    planetList.push( {
        id: id,
        name: name,
        size: size,
        dateDiscovered: dateDiscovered
    });
    
    resp.json(planetList);
    resp.end();
});

app.put('/edit', (req, resp) => {
    let { id, name, size, dateDiscovered } = req.body;

    planetList[id] = {
        id: id,
        name: name,
        size: size,
        dateDiscovered: dateDiscovered
    }
    resp.json(planetList);
    resp.end();
});

app.listen(port, () => {
    console.log('server started on port 8080');
});